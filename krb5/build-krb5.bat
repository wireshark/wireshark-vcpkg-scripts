rem @echo off

rem To do: Convert this to GitLab CI YAML.

rem Instructions:
rem - Make sure the "Visual C++ ATL for x86 and x64" and "Visual C++ MFC
rem   for x86 and x64" Visual Studio components are installed.
rem - Adjust GIT_BIN_PATH and PERL_BIN_PATH as needed.
rem - Update KRB_VERSION and PKG_VERSION as needed.
rem - Run this from a fresh command prompt.

rem Running from a fresh command prompt is recommended.

set KRB_VERSION=1.21.3
set PKG_VERSION=-1
set KRB_WS_DIR=%~dp0
set KRB_GIT_DIR=%KRB_WS_DIR%\krb5
set KRB_SRC_DIR=%KRB_GIT_DIR%\src
set KRB_X64_WINDOWS_BUNDLE_DIR=krb5-%KRB_VERSION%%PKG_VERSION%-x64-windows-ws
set KRB_ARM64_WINDOWS_BUNDLE_DIR=krb5-%KRB_VERSION%%PKG_VERSION%-arm64-windows-ws

set GIT_BIN_PATH=C:\Program Files\Git\usr\bin
set PERL_BIN_PATH=C:\straberry\perl\bin
set PATH=%PATH%;C:\Program Files (x86)\HTML Help Workshop;%PERL_BIN_PATH%;%GIT_BIN_PATH%

: Try to trim our build
set NO_LEASH=1

cd %KRB_WS_DIR% || goto error
if not exist %KRB_GIT_DIR%\.git (
    echo Cloning krb5 repository...
    rm -rf %KRB_GIT_DIR% || goto error
    git clone https://github.com/krb5/krb5.git || goto error
)

echo Cloning and checking out version %KRB_VERSION%...
cd %KRB_GIT_DIR%
git clean -dffx || goto error
git reset --hard HEAD || goto error
git checkout krb5-%KRB_VERSION%-final || goto error
cd %KRB_WS_DIR% || goto error

echo Removing bundle directories and archives...
rm -rf ^
    %KRB_X64_WINDOWS_BUNDLE_DIR% ^
    %KRB_X64_WINDOWS_BUNDLE_DIR%.zip ^
    %KRB_ARM64_WINDOWS_BUNDLE_DIR% ^
    %KRB_ARM64_WINDOWS_BUNDLE_DIR%.zip ^
    || goto error

echo Setting up Visual Studio x64 environment...
call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

echo Starting NODEBUG x64 build.
set KRB_INSTALL_DIR=%KRB_WS_DIR%\%KRB_X64_WINDOWS_BUNDLE_DIR%\installed\x64-windows
mkdir %KRB_INSTALL_DIR% || goto error
cd %KRB_SRC_DIR% || goto error
nmake -f Makefile.in prep-windows || goto error
nmake NODEBUG=1 || goto error
nmake install NODEBUG=1 || goto error

cd %KRB_GIT_DIR%
git clean -dffx || goto error
git reset --hard HEAD || goto error

echo Starting DEBUG x64 build.
set KRB_INSTALL_DIR=%KRB_WS_DIR%\%KRB_X64_WINDOWS_BUNDLE_DIR%\installed\x64-windows\debug
mkdir %KRB_INSTALL_DIR% || goto error
cd %KRB_SRC_DIR% || goto error
nmake -f Makefile.in prep-windows || goto error
nmake || goto error
nmake install || goto error
rm -rf %KRB_INSTALL_DIR%\include || goto error

cd %KRB_WS_DIR%
cp -vf build-krb5.bat README.wireshark %KRB_X64_WINDOWS_BUNDLE_DIR% || goto error
7z a -tzip %KRB_X64_WINDOWS_BUNDLE_DIR%.zip %KRB_X64_WINDOWS_BUNDLE_DIR% || goto error

cd %KRB_GIT_DIR%
git clean -dffx || goto error
git reset --hard HEAD || goto error

echo Setting up Visual Studio arm64 environment...
call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" arm64

echo Starting NODEBUG arm64 build.
set KRB_INSTALL_DIR=%KRB_WS_DIR%\%KRB_ARM64_WINDOWS_BUNDLE_DIR%\installed\arm64-windows
mkdir %KRB_INSTALL_DIR% || goto error
cd %KRB_SRC_DIR% || goto error
nmake -f Makefile.in prep-windows || goto error
nmake NODEBUG=1 || goto error
nmake install NODEBUG=1 || goto error

cd %KRB_GIT_DIR%
git clean -dffx || goto error
git reset --hard HEAD || goto error

echo Starting DEBUG arm64 build.
set KRB_INSTALL_DIR=%KRB_WS_DIR%\%KRB_ARM64_WINDOWS_BUNDLE_DIR%\installed\arm64-windows\debug
mkdir %KRB_INSTALL_DIR% || goto error
cd %KRB_SRC_DIR% || goto error
nmake -f Makefile.in prep-windows || goto error
nmake || goto error
nmake install || goto error
rm -rf %KRB_INSTALL_DIR%\include || goto error

cd %KRB_WS_DIR%
cp -vf build-krb5.bat README.wireshark %KRB_ARM64_WINDOWS_BUNDLE_DIR% || goto error
7z a -tzip %KRB_ARM64_WINDOWS_BUNDLE_DIR%.zip %KRB_ARM64_WINDOWS_BUNDLE_DIR% || goto error

sha256sum %KRB_X64_WINDOWS_BUNDLE_DIR%.zip %KRB_ARM64_WINDOWS_BUNDLE_DIR%.zip

goto :EOF

:error
echo Error %errorlevel%
exit /b %errorlevel%
