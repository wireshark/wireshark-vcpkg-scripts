# Build libsinsp and libscap

falcosecurity-libs:
  # It would be nice to be able to use a more official image, but Martin Storsjö's
  # repository is listed at https://www.mingw-w64.org/downloads/
  extends: .build-vcpkg
  stage: build
  variables:
    PKG_VERSION: "0.18.1"
    WS_REVISION: "1"
  rules:
  - if: '$CI_PIPELINE_SOURCE == "push"'
    when: manual
    allow_failure: true
  - if: '$CI_PIPELINE_SOURCE == "push"'
    changes:
      - falcosecurity-libs
    when: always
  script:
    - $env:Configuration = "RelWithDebInfo"
    # .build-vcpkg leaves us in C:\vcpkg
    - cd "${CI_PROJECT_DIR}"
    - git clone https://github.com/falcosecurity/libs falcosecurity-libs-build
    - cd falcosecurity-libs-build
    - git switch -c ${PKG_VERSION} ${PKG_VERSION}
    # https://help.appveyor.com/discussions/questions/18777-how-to-use-vcvars64bat-from-powershell
    - cmd.exe /c "call `"C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat`" arm64 && set > %temp%\vcvars.txt"
    - Get-Content "$env:temp\vcvars.txt" | Foreach-Object { if ($_ -match "^(.*?)=(.*)$") { Set-Content "env:\$($matches[1])" $matches[2] } }
    - mkdir build-arm64
    - cd build-arm64
    - cmake -A arm64 -G "Visual Studio 17 2022" -DCMAKE_MSVC_RUNTIME_LIBRARY=MultiThreadedDLL -DMINIMAL_BUILD=ON -DUSE_BUNDLED_DEPS=ON -DBUILD_LIBSINSP_EXAMPLES=OFF -DCREATE_TEST_TARGETS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX="${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws/installed/arm64-windows" ..
    - msbuild /m falcosecurity-libs.sln
    - msbuild /m INSTALL.vcxproj
    - copy uthash-prefix\src\uthash\src\uthash.h "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws\installed\arm64-windows\include\falcosecurity\"
    - cd ..
    - cmd.exe /c "call `"C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat`" x64 && set > %temp%\vcvars.txt"
    - Get-Content "$env:temp\vcvars.txt" | Foreach-Object { if ($_ -match "^(.*?)=(.*)$") { Set-Content "env:\$($matches[1])" $matches[2] } }
    - mkdir build-x64
    - cd build-x64
    - cmake -A x64 -G "Visual Studio 17 2022"-DCMAKE_MSVC_RUNTIME_LIBRARY=MultiThreadedDLL -DMINIMAL_BUILD=ON -DUSE_BUNDLED_DEPS=ON -DBUILD_LIBSINSP_EXAMPLES=OFF -DCREATE_TEST_TARGETS=OFF -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_INSTALL_PREFIX="${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws/installed/x64-windows" ..
    - msbuild /m falcosecurity-libs.sln
    - msbuild /m INSTALL.vcxproj
    - copy uthash-prefix\src\uthash\src\uthash.h "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws\installed\x64-windows\include\falcosecurity\"
    - cd "${CI_PROJECT_DIR}"
    - 7z a -tzip "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws.zip" "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws"
    - 7z a -tzip "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws.zip" "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws"
    - Get-ChildItem -path ${PORT_NAME}-*.zip | Get-Filehash -algorithm SHA256
  artifacts:
    name: "$CI_JOB_NAME-$PKG_VERSION"
    paths:
      - ${PORT_NAME}-*.zip
  needs: []

# I initially tried building using go and zig https://stackoverflow.com/a/67927465/82195
# but didn't end up with runnable Arm64 DLLs.
falcosecurity-plugins:
  image: mstorsjo/llvm-mingw
  stage: build
  variables:
    PLUGINS_COMMIT: "2277f83"
    # PKG_VERSION is derived from the commit date below.
    WS_REVISION: "2"
    GO_VERSION: "1.23.2"
  rules:
  - if: '$CI_PIPELINE_SOURCE == "push"'
    when: manual
    allow_failure: true
  - if: '$CI_PIPELINE_SOURCE == "push"'
    changes:
      - falcosecurity-libs
  script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt-get update
    - apt-get --yes install 7zip coreutils curl git make xz-utils
    - curl -JLO https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz
    - tar -xf go${GO_VERSION}.linux-amd64.tar.gz
    - export PATH=$PATH:${CI_PROJECT_DIR}/go/bin
    - git clone https://github.com/falcosecurity/plugins.git falcosecurity-plugins
    - cd falcosecurity-plugins/plugins
    - git checkout $PLUGINS_COMMIT
    - PKG_VERSION=$( git show --no-patch --format=%cd --date=short HEAD )
    - mkdir "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws"
    - mkdir "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws"
    - export CGO_ENABLED=1
    - export GOOS=windows
    # Arm64
    - export GOARCH=arm64
    - export CC=/opt/llvm-mingw/bin/aarch64-w64-mingw32-gcc
    - export CXX=/opt/llvm-mingw/bin/aarch64-w64-mingw32-g++
    - cd cloudtrail
    - make clean
    - make
    - mv libcloudtrail.so "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws/cloudtrail.dll"
    - cd ../gcpaudit
    - make clean
    - make
    - mv libgcpaudit.so "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws/gcpaudit.dll"
    # x64
    - export GOARCH=amd64
    - export CC=/opt/llvm-mingw/bin/x86_64-w64-mingw32-gcc
    - export CXX=/opt/llvm-mingw/bin/x86_64-w64-mingw32-g++
    - cd ../cloudtrail
    - make clean
    - make
    - mv libcloudtrail.so "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws/cloudtrail.dll"
    - cd ../gcpaudit
    - make clean
    - make
    - mv libgcpaudit.so "${CI_PROJECT_DIR}/${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws/gcpaudit.dll"
    - cd "${CI_PROJECT_DIR}"
    - cp falcosecurity-libs/README.plugins.md ${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws/README.md
    - cp falcosecurity-libs/README.plugins.md ${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws/README.md
    - 7zz a -tzip "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws.zip" "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-arm64-ws"
    - 7zz a -tzip "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws.zip" "${PORT_NAME}-${PKG_VERSION}-${WS_REVISION}-x64-ws"
    - sha256sum ${PORT_NAME}-*.zip
  artifacts:
    name: "$CI_JOB_NAME-$PLUGINS_COMMIT-$WS_REVISION"
    paths:
      - ${PORT_NAME}-*.zip
  needs: []
