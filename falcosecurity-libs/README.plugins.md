# Falco plugins for Windows

This archive contains a collection of plugins built for Windows from https://github.com/falcosecurity/plugins/ using go and [LLVM+MinGW](https://github.com/mstorsjo/llvm-mingw/).

It currently contains the cloudtrail plugin.
More plugins are expected to be added in the future.
