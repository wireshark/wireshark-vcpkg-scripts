#!/usr/bin/env python3

import argparse
import os
import re

# To use this, you must first download and extract
# https://repo.msys2.org/mingw/clang64/clang64.db.tar.zst
# and https://repo.msys2.org/mingw/clang64/clangarm64.db.tar.zst

packages = []

def get_package_info(package, db_dir):
    print(f'Getting dependency info for {package}')
    pkg_re = re.compile(rf'^{package}-\d+.*-\d+')
    matches = [p for p in packages if pkg_re.match(p)]
    if len(matches) < 1:
        print(f'No matching package for {package}')
        return None
    if len(matches) > 1:
        print(f'Multiple matching package for {package}: {matches}')
        return None
    pkg_dir = matches[0]

    desc_parts = {}
    with open(os.path.join(db_dir, pkg_dir, 'desc'), 'r') as desc_f:
        in_part = None
        for dl in desc_f.readlines():
            dl = dl.strip()
            if dl.startswith('%'):
                in_part = dl
                desc_parts[in_part] = []
            elif dl == '':
                in_part = None
            elif in_part:
                desc_parts[in_part].append(dl)
        return { package: {
            'filename': desc_parts[r'%FILENAME%'][0],
            'sha256': desc_parts[r'%SHA256SUM%'][0]
        }}

    return {}

def main():
    parser = argparse.ArgumentParser(description='MSYS2 package information fetcher')
    parser.add_argument('--database-directory', '-d', metavar='<database directory>', help='database directory')
    parser.add_argument("package_names", metavar='package_names', nargs='+', help="package names")
    args = parser.parse_args()

    packages.extend(os.listdir(args.database_directory))

    arm64_deps = {}
    for package in args.package_names:
        arm64_deps.update(get_package_info('mingw-w64-clang-aarch64-' + package, args.database_directory))
    pkg_l = sorted(arm64_deps.keys())
    print('      arm64_urls=(')
    for pkg in pkg_l:
        print(f'        https://mirror.msys2.org/mingw/clangarm64/{arm64_deps[pkg]["filename"]}')
    print('      )\n')

    x64_deps = {}
    for package in args.package_names:
        x64_deps.update(get_package_info('mingw-w64-clang-x86_64-' + package, args.database_directory))
    pkg_l = sorted(x64_deps.keys())
    print('      x64_urls=(')
    for pkg in pkg_l:
        print(f'        https://mirror.msys2.org/mingw/clang64/{x64_deps[pkg]["filename"]}')
    print('      )\n')

    pkg_l = sorted(arm64_deps.keys())
    for pkg in pkg_l:
        print(f'      {arm64_deps[pkg]["sha256"]}  {arm64_deps[pkg]["filename"]}')

    pkg_l = sorted(x64_deps.keys())
    for pkg in pkg_l:
        print(f'      {x64_deps[pkg]["sha256"]}  {x64_deps[pkg]["filename"]}')


if __name__ == "__main__":
    main()
