#!/bin/bash

REPO_DIR="$( dirname "$0" )/.."

SINGLE_PACKAGES=()

while read -r PKG_GLCI ; do
    PKG_GLCI=${PKG_GLCI%/gitlab-ci.yml}
    PKG_GLCI=${PKG_GLCI##*/}
    SINGLE_PACKAGES+=("${PKG_GLCI%/gitlab-ci.yml}")
done < <( grep -l "extends: *.build-vcpkg-single" "$REPO_DIR"/*/gitlab-ci.yml )

printf "\n"
printf "vcpkg single\n"
printf "%-15s %10s %10s\n" "Package" "Our ver" "vcpkg ver"

for PKG in "${SINGLE_PACKAGES[@]}" ; do
    OUR_VER=$(grep '^  *PKG_VERSION' "$REPO_DIR/$PKG/gitlab-ci.yml" | awk '{gsub(/"/, "", $2); print $2}')
    OUR_VER=${OUR_VER%-*}

    VCPKG_VER=$(
        curl --silent "https://raw.githubusercontent.com/microsoft/vcpkg/master/ports/$PKG/vcpkg.json" |
            jq --raw-output '.version // ."version-string" // ."version-semver"'
    )
    VCPKG_VER=${VCPKG_VER%\"}
    VCPKG_VER=${VCPKG_VER#\"}

    APPEND=""
    if [ "$OUR_VER" != "$VCPKG_VER" ] ; then
        APPEND=" ⬅︎"
    fi

    printf "%-15s %10s %10s%s\n" "$PKG" "$OUR_VER" "$VCPKG_VER" "$APPEND"
done

# vcpkg-export
CURRENT_VCPKG_TAG=$(awk '/CURRENT_VCPKG_TAG:/ {gsub(/"/, "", $2); print $2}' "$REPO_DIR/.gitlab-ci.yml")
VE_COMMIT=$(awk '/PORT_VCPKG_COMMIT:/ {gsub(/"/, "", $2); print $2}' "$REPO_DIR/vcpkg-export/gitlab-ci.yml")

if [[ $VE_COMMIT == *CURRENT_VCPKG_TAG* ]] ; then
    VE_COMMIT=$CURRENT_VCPKG_TAG
fi

printf "\n"
printf "vcpkg export\n"
printf "%-15s %10s %10s\n" "Package" "Our ver" "vcpkg ver"

# XXX Add libiconv and zlib when we have a commit that includes vcpkg.json.
for PKG in gettext glib libxml2 ; do
    OUR_VER=$(
        curl --silent "https://raw.githubusercontent.com/microsoft/vcpkg/$VE_COMMIT/ports/$PKG/vcpkg.json" |
            jq --raw-output '.version // ."version-string" // ."version-semver"'
    )

    VCPKG_VER=$(
        curl --silent "https://raw.githubusercontent.com/microsoft/vcpkg/master/ports/$PKG/vcpkg.json" |
            jq --raw-output '.version // ."version-string" // ."version-semver"'
    )

    APPEND=""
    if [ "$OUR_VER" != "$VCPKG_VER" ] ; then
        APPEND=" ⬅︎"
    fi

    printf "%-15s %10s %10s%s\n" "$PKG" "$OUR_VER" "$VCPKG_VER" "$APPEND"
done

# release-monitoring.org
RMO_PACKAGES=()

while read -r PKG_GLCI ; do
    PKG_GLCI=${PKG_GLCI%/gitlab-ci.yml}
    PKG_GLCI=${PKG_GLCI##*/}
    RMO_PACKAGES+=("${PKG_GLCI%/gitlab-ci.yml}")
done < <( grep -l '# release-monitoring-project-id:' "$REPO_DIR"/*/gitlab-ci.yml )

printf "\n"
printf "release-monitoring.org\n"
printf "%-15s %10s %10s\n" "Package" "Our ver" "r-m.org ver"

for PKG in "${RMO_PACKAGES[@]}" ; do
    OUR_VER=$(grep '^  *PKG_VERSION:' "$REPO_DIR/$PKG/gitlab-ci.yml" | awk '{gsub(/"/, "", $2); print $2}')
    OUR_VER=${OUR_VER%-*}

    RMO_PROJ=$(grep '# release-monitoring-project-id:' "$REPO_DIR/$PKG/gitlab-ci.yml" | awk '{print $3}')

    if [ -z "$RMO_PROJ" ] ; then
        continue
    fi

    RMO_VER=$(
        curl --silent "https://release-monitoring.org/api/v2/versions/?project_id=$RMO_PROJ" |
            jq --raw-output '.latest_version'
    )

    APPEND=""
    if [ "$OUR_VER" != "$RMO_VER" ] ; then
        APPEND=" ⬅︎"
    fi

    printf "%-15s %10s %10s%s\n" "$PKG" "$OUR_VER" "$RMO_VER" "$APPEND"
done
